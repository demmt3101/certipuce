from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Events(models.Model):
    id_event = models.PositiveSmallIntegerField(primary_key=True)
    nombre = models.CharField(max_length=30)
    fecha = models.CharField(max_length=30)
    hora = models.CharField(max_length=30)
    lugar = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre + "--" + self.fecha


class Person(models.Model):
    id = models.CharField(max_length=8, primary_key=True)
    nombres = models.CharField(max_length=35)
    apellidos = models.CharField(max_length=35)
    email = models.CharField(max_length=100)
