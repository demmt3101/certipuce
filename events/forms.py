from django.forms import ModelForm
from .models import Events


class TaskForm(ModelForm):
    class Meta:
        model = Events
        fields = ["id_event", "nombre", "fecha", "hora", "lugar", "descripcion"]
