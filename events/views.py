from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from .models import Events
from .forms import TaskForm
from django.contrib import messages


# Create your views here.
def base(request):
    return render(request, "base.html")


def index(request):
    return render(request, "index.html")


def crear_usuario(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            try:
                user = form.save()
                login(request, user)
                messages.success(request, "User created successfully!")
                return redirect("pl")
            except IntegrityError:
                messages.error(request, "Username already exists.")
        else:
            messages.error(request, "Please correct the error below.")
    else:
        form = UserCreationForm()

    return render(request, "crear_usuario.html", {"form": form})


def lista_eventos(request):
    events = Events.objects.all()
    return render(request, "procesar_login.html", {"events": events})


def events_completed(request):
    events = Events.objects.filter(
        user=request.user, datecompleted__isnull=False
    ).order_by("-datecompleted")
    return render(request, "tasks.html", {"tasks": events})


def crear_evento(request):
    if request.method == "GET":
        return render(request, "crear_evento.html", {"form": TaskForm})
    else:
        try:
            form = TaskForm(request.POST)
            new_event = form.save(commit=False)
            new_event.user = request.user
            new_event.save()
            return redirect("ap")
        except ValueError:
            return render(
                request,
                "crear_evento.html",
                {"form": TaskForm, "error": "Error creating task."},
            )


def home(request):
    return render(request, "home.html")


def signout(request):
    logout(request)
    return redirect("home")


def procesar_login(request):
    if request.method == "GET":
        return render(request, "procesar_login.html", {"form": AuthenticationForm})
    else:
        user = authenticate(
            request,
            username=request.POST["username"],
            password=request.POST["password"],
        )
        if user is None:
            return render(
                request,
                "base.html",
                {
                    "form": AuthenticationForm,
                    "error": "Username or password is incorrect.",
                },
            )

        login(request, user)
        return redirect("pl")


def admin_paricipantes(request):
    if request.method == "GET":
        return render(request, "admin_participantes.html", {"form": AuthenticationForm})
    else:
        user = authenticate(
            request,
            username=request.POST["username"],
            password=request.POST["password"],
        )
        if user is None:
            return render(
                request,
                "index_admin.html",
                {
                    "form": AuthenticationForm,
                    "error": "Username or password is incorrect.",
                },
            )

        login(request, user)
        return redirect("ap")


def detalles_evento(request, id_event):
    if request.method == "GET":
        event = get_object_or_404(Events, pk=id_event, user=request.user)
        form = TaskForm(instance=event)
        return render(request, "detalles_evento.html", {"event": event, "form": form})
    else:
        try:
            event = get_object_or_404(Events, pk=id_event, user=request.user)
            form = TaskForm(request.POST, instance=event)
            form.save()
            return redirect("lista_eventos")
        except ValueError:
            return render(
                request,
                "detalles_evento.html",
                {"event": event, "form": form, "error": "Error updating task."},
            )


def complete_event(request, id_event):
    event = get_object_or_404(Events, pk=id_event, user=request.user)
    if request.method == "POST":
        event.datecompleted = timezone.now()
        event.save()
        return redirect("pl")


def delete_task(request, id_event):
    task = get_object_or_404(Events, pk=id_event, user=request.user)
    if request.method == "POST":
        task.delete()
        return redirect("ap")


def index_admin(request):
    return render(request, "index_admin.html")
